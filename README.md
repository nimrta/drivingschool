## Driving School Project

An ASP.NET Razor based project on driving school system.

## Prerequisites to use this project

1. .NET Core SDK
2. Visual Studio Code
3. C# extension

## How to run this project

To run the application:

1. Clone the project repository.
    a. You can use Visual Studio, Visual Studio Code, GitHub Desktop, etc.
    b. You can download the ZIP file of the repository.
2. Run the application.

## License and Copyright

Licensed under the [MIT License](LICENSE).

NOTE: The MIT licence is an excellent choice since it allows you to distribute your code under a copyleft licence without requiring others to divulge their proprietary code, it's business friendly, and it allows for monetization.