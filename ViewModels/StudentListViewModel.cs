﻿using DrivingSchool.Models.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DrivingSchool.ViewModels
{
    public class StudentListViewModel
    {
        public List<Student> Students { get; set; }
        
    }
}
