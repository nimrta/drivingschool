﻿using DrivingSchool.Models.EntityModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DrivingSchool.ViewModels
{
    public class StudentViewModel
    {
        public int StudentID { get; set; }
        [Required(ErrorMessage = "First name is required")]
        [DisplayName("First Name")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last name is required")]
        [DisplayName("Last Name")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Address is required")]
        [DisplayName("Address")]
        public string Address { get; set; }
        [Required(ErrorMessage = "City is required")]
        [DisplayName("City")]
        public string City { get; set; }

        [BindProperty, DataType(DataType.PostalCode)]
        [Required(ErrorMessage = "Postal code is required")]
        [RegularExpression(@"^((([A-PR-UWYZ][0-9])|([A-PR-UWYZ][0-9][0-9])|([A-PR-UWYZ][A-HK-Y][0-9])|([A-PR-UWYZ][A-HK-Y][0-9][0-9])|([A-PR-UWYZ][0-9][A-HJKSTUW])|([A-PR-UWYZ][A-HK-Y][0-9][ABEHMNPRVWXY]))\s?([0-9][ABD-HJLNP-UW-Z]{2})|(GIR)\s?(0AA))$", ErrorMessage = "Not a valid postal code as \"LL00 0LL \" ")]
        
        [DisplayName("Poatal Code")]
        public string PostalCode { get; set; }

        [BindProperty, DataType(DataType.PhoneNumber)]
        [Required(ErrorMessage = "Phone is required")]
        [RegularExpression(@"^(?:(?:(?:00\s?|\+)44\s?|0)(?:1\d{8,9}|[23]\d{9}|7(?:[1345789]\d{8}|624\d{6})))$", ErrorMessage = "Not a valid phone number as \"+447888888883\"")]
        [DisplayName("Phone")]
        public string Phone { get; set; }

        [BindProperty, DataType(DataType.Date)]
        [Required(ErrorMessage = "Start date is required")]
        [DisplayName("Start Date")]
        public DateTime StartDate { get; set; }

        [BindProperty, DataType(DataType.Date)]
        [DisplayName("Gratiation Date")]
        public DateTime? GraduationDate { get; set; }

        public int AdmissionMode { get; set; }

        [Required(ErrorMessage = "Admission mode is required")]
        [DisplayName("Admission Mode")]
        public int AdmissionModeID { get; set; }
        [Required(ErrorMessage = "Car type is required")]
        [DisplayName("Car Type")]
        public int CarTypeID { get; set; }
        [Required(ErrorMessage = "Instructor is required")]
        [DisplayName("Instructor")]
        public int InstructorID { get; set; }

        private List<AdmissionMode> admissionModes;
        public List<AdmissionMode> AdmissionModes
        {
            get
            {
                return admissionModes;
            }
            set
            {
                admissionModes = new List<AdmissionMode>();
                admissionModes.AddRange(value);
            }
        }

        private List<CarType> carTypes;
        public List<CarType> CarTypes
        {
            get { return carTypes; }
            set
            {
                carTypes = new List<CarType>();
                carTypes.AddRange(value);
            }
        }

        private List<Instructor> instructors;
        public List<Instructor> Instructors
        {
            get { return instructors; }
            set
            {
                instructors = new List<Instructor>();
                instructors.AddRange(value);
            }
        }

    }
}


