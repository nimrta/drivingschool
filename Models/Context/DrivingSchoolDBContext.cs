﻿using DrivingSchool.Models.EntityModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DrivingSchool.Models.Context
{
    public class DrivingSchoolDBContext : DbContext
    {
        public DrivingSchoolDBContext(DbContextOptions<DrivingSchoolDBContext> options)
           : base(options)
        { }

        public DbSet<Student> Students { get; set; }
        public DbSet<Instructor> Instructors { get; set; }
        public DbSet<AdmissionMode> AdmissionModes { get; set; }
        public DbSet<CarType> CarTypes { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Instructor>().HasData(
                new Instructor { InstructorID = 1, InstructorName = "John Smith" },
                new Instructor { InstructorID = 2, InstructorName = "Allen" },
                new Instructor { InstructorID = 3, InstructorName = "William" }
            );
            modelBuilder.Entity<AdmissionMode>().HasData(
                new AdmissionMode { AdmissionModeID = 1, AdmissionModeName = "Full Time" },
                new AdmissionMode { AdmissionModeID = 2, AdmissionModeName = "Part Time" },
                new AdmissionMode { AdmissionModeID = 3, AdmissionModeName = "Graduated" }

            );
            modelBuilder.Entity<CarType>().HasData(
                new CarType { CarTypeID = 1, CarTypeName = "Own Car" },
                new CarType { CarTypeID = 2, CarTypeName = "Instructor's Car" },
                new CarType { CarTypeID = 3, CarTypeName = "School's Car" }
            );
        }
    }
}
