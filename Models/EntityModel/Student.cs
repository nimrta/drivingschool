﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DrivingSchool.Models.EntityModel
{
    public class Student
    {
        [Key]
        public int StudentID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? GraduationDate { get; set; }

        public int AdmissionModeID { get; set; }
        [ForeignKey("AdmissionModeID")]
        public AdmissionMode AdmissionMode { get; set; }

        public int CarTypeID { get; set; }
        [ForeignKey("CarTypeID")]
        public CarType CarType { get; set; }

        public int InstructorID { get; set; }
        [ForeignKey("InstructorID")]
        public Instructor Instructor { get; set; }
    }
}
