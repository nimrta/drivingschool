﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DrivingSchool.Models.EntityModel
{
    public class AdmissionMode
    {
        [Key]
        public int AdmissionModeID { get; set; }
        public string AdmissionModeName { get; set; }

    }
}
