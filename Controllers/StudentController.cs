﻿using DrivingSchool.Models.Context;
using DrivingSchool.Models.EntityModel;
using DrivingSchool.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DrivingSchool.Controllers
{
    public class StudentController : Controller
    {
        private DrivingSchoolDBContext context;
        private readonly ILogger<HomeController> _logger;
        // GET: StudentController

        public StudentController(ILogger<HomeController> logger, DrivingSchoolDBContext ctx)
        {
            _logger = logger;
            context = ctx;
        }

        public ActionResult Index()
        {
            return View();
        }

        // GET: StudentController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: StudentController/Create
        public ActionResult Create()
        {
            ViewBag.Title = "Add Student data";
            StudentViewModel studentViewModel = new StudentViewModel();

            List<CarType> carTypes = context.CarTypes.ToList();
            List<AdmissionMode> admissionModes = context.AdmissionModes.ToList();
            List<Instructor> instructors = context.Instructors.ToList();

            studentViewModel.AdmissionModes = admissionModes;
            studentViewModel.CarTypes = carTypes;
            studentViewModel.Instructors = instructors;

            return View(studentViewModel);
        }

        // POST: StudentController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: StudentController/Edit/5
        public ActionResult Edit(int id)
        {
            ViewBag.Title = "Edit Student data";
            var student = context.Students.Include(s => s.AdmissionMode)
                                        .Include(s => s.Instructor)
                                        .Include(s => s.CarType)
                                        .Where(s => s.StudentID == id).FirstOrDefault();
            if (student == null)
                student = new Student();

            StudentViewModel studentViewModel = new StudentViewModel();

            studentViewModel.StudentID = student.StudentID;
            studentViewModel.FirstName = student.FirstName;
            studentViewModel.LastName = student.LastName;
            studentViewModel.Address = student.Address;
            studentViewModel.City = student.City;
            studentViewModel.PostalCode = student.PostalCode;
            studentViewModel.Phone = student.Phone;
            studentViewModel.StartDate = student.StartDate;
            studentViewModel.GraduationDate = student.GraduationDate;
            studentViewModel.InstructorID = student.InstructorID;
            studentViewModel.CarTypeID = student.CarTypeID;
            studentViewModel.AdmissionModeID = student.AdmissionModeID;

            List<CarType> carTypes = context.CarTypes.ToList();
            List<AdmissionMode> admissionModes = context.AdmissionModes.ToList();
            List<Instructor> instructors = context.Instructors.ToList();

            studentViewModel.AdmissionModes = admissionModes;
            studentViewModel.CarTypes = carTypes;
            studentViewModel.Instructors = instructors;

            return View("Create", studentViewModel);
        }

        // POST: StudentController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(StudentViewModel studentViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var student = context.Students.Include(s => s.AdmissionMode)
                                           .Include(s => s.Instructor)
                                           .Include(s => s.CarType)
                                           .Where(s => s.StudentID == studentViewModel.StudentID).FirstOrDefault();

                    if (student == null)
                        student = new Student();

                    //Student student = new Student();
                    student.FirstName = studentViewModel.FirstName;
                    student.LastName = studentViewModel.LastName;
                    student.Address = studentViewModel.Address;
                    student.City = studentViewModel.City;
                    student.PostalCode = studentViewModel.PostalCode;
                    student.Phone = studentViewModel.Phone;
                    student.StartDate = studentViewModel.StartDate;
                    student.GraduationDate = studentViewModel.GraduationDate;
                    student.InstructorID = studentViewModel.InstructorID;
                    student.CarTypeID = studentViewModel.CarTypeID;
                    student.AdmissionModeID = studentViewModel.AdmissionModeID;

                    if (student.StudentID > 0)
                    {
                        context.SaveChanges();
                    }
                    else
                    {
                        context.Students.Add(student);
                    }
                    context.SaveChanges();

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("Invalid Model", "Invalid Model");
                    return View("Create", studentViewModel);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", "Error occured when saving.");
                return View("Create", studentViewModel);
            }
        }

        // GET: StudentController/Delete/5
        public ActionResult Delete(int id)
        {
            var student = context.Students.Include(s => s.AdmissionMode)
                                       .Include(s => s.Instructor)
                                       .Include(s => s.CarType)
                                       .Where(s => s.StudentID == id).FirstOrDefault();
            if (student != null)
            {
                context.Students.Remove(student);
                context.SaveChanges();

                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        // POST: StudentController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
