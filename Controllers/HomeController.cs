﻿using DrivingSchool.Models;
using DrivingSchool.Models.Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DrivingSchool.ViewModels;
using DrivingSchool.Models.EntityModel;

namespace DrivingSchool.Controllers
{
    public class HomeController : Controller
    {
        private DrivingSchoolDBContext context;
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger, DrivingSchoolDBContext ctx)
        {
            _logger = logger;
            context = ctx;
        }

        public IActionResult Index()
        {
            StudentListViewModel studentListViewModel = new StudentListViewModel();
            try
            {
                List<Student> Students = context.Students.Include(s => s.AdmissionMode)
                                        .Include(s => s.Instructor)
                                        .ToList();

                if (Students == null)
                    Students = new List<Student>();

                studentListViewModel.Students = Students;
            }
            catch(Exception ex)
            {
                _logger.LogError(ex,"");
            }
            return View(studentListViewModel);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
